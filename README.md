# CBEN 624
Interactive Python exercises for CBEN 624: Applied Statistical Mechanics (2021, Colorado School of Mines).
The content in these exercises have been adapted from other courses (see notes in each section).

To launch an interactive session (via Binder): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/pak-group%2Fcben-624/HEAD)

Outline:
- Prelim, python-introduction
  - Read and run all of the items in python-introduction/python-tutorial.ipynb
- HW4, intro-to-mc-and-md
  - Fill in the missing pieces in HW4/intro-to-mc-and-md.ipynb
